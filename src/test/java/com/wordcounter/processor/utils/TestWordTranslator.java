package com.wordcounter.processor.utils;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import com.wordcounter.processor.base.TestBaseSetup;

@PrepareForTest({ WordTranslator.class })
public class TestWordTranslator extends TestBaseSetup {

	@Test
	public void testGetTraslatedEnglishWordForNonEnglishWord() throws Exception {
		failedTestCaseList.add("TestWordTranslator.testGetTraslatedEnglishWordForNonEnglishWord()");
		
		String word = "silla";
		String expectedRetuen = "chair";

		// Test Case 1
		PowerMockito.when(wordTranslatorMock.googleTranslatorApiLookUpToWordIntoEnglish(Mockito.anyString(),
				Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn(expectedRetuen);

		String translatedWord = wordTranslatorMock.getTraslatedEnglishWordForNonEnglishWord(word, googleApiKey);
		assertEquals("chair", translatedWord);

		// Test Case 2
		PowerMockito.when(wordTranslatorMock.googleTranslatorApiLookUpToWordIntoEnglish(Mockito.anyString(),
				Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn(null);

		try {
			translatedWord = wordTranslatorMock.getTraslatedEnglishWordForNonEnglishWord(word, googleApiKey);
		} catch (Exception ex) {
			assertEquals("Google translation api translated  null or empty value for given word '" + word + "'",
					ex.getMessage());
		}
		
		failedTestCaseList.add("TestWordTranslator.testGetTraslatedEnglishWordForNonEnglishWord()");
	}

}
