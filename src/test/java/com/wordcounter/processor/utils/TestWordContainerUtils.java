package com.wordcounter.processor.utils;


import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import com.wordcounter.processor.base.TestBaseSetup;


public class TestWordContainerUtils extends TestBaseSetup{
  
	@Test
	public void testIsAlphabeticWord(){
		failedTestCaseList.add("TestWordContainerUtils.testIsAlphabeticWord()");
		
		assertEquals(false, WordContainerUtils.isAlphabeticWord(null));
		assertEquals(false, WordContainerUtils.isAlphabeticWord(""));
		assertEquals(false, WordContainerUtils.isAlphabeticWord("chair123"));
		assertEquals(false, WordContainerUtils.isAlphabeticWord("chair@123"));
		assertEquals(false, WordContainerUtils.isAlphabeticWord("chair$"));
		assertEquals(false, WordContainerUtils.isAlphabeticWord("12344"));
		assertEquals(false, WordContainerUtils.isAlphabeticWord("%^#$"));
		assertEquals(true, WordContainerUtils.isAlphabeticWord("CHAIR"));
		assertEquals(true, WordContainerUtils.isAlphabeticWord("Table"));
		assertEquals(true, WordContainerUtils.isAlphabeticWord("wallet"));
		
		failedTestCaseList.remove("TestWordContainerUtils.testIsAlphabeticWord()");

	}
	
	@Test
	public void testwordTranslatoinLookUp() throws Exception{
		failedTestCaseList.add("TestWordContainerUtils.testwordTranslatoinLookUp()");
		
		assertEquals("tray", WordContainerUtils.wordTranslatoinLookUp("bandeja", wordContainerComposer, googleApiKey));
		
		String expectedRetuen = "train";

		PowerMockito.when(wordTranslatorMock.googleTranslatorApiLookUpToWordIntoEnglish(Mockito.anyString(),
				Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn(expectedRetuen);
		
		assertEquals("train", WordContainerUtils.wordTranslatoinLookUp("tren", wordContainerComposer, googleApiKey));
		
		failedTestCaseList.remove("TestWordContainerUtils.testwordTranslatoinLookUp()");
	}
}
