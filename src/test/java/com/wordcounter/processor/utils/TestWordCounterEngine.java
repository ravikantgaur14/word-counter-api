package com.wordcounter.processor.utils;

import static org.junit.Assert.assertEquals;
import java.util.HashMap;
import java.util.Map;
import org.junit.Test;
import com.wordcounter.processor.base.TestBaseSetup;

public class TestWordCounterEngine extends TestBaseSetup {

	WordCounterEngine wordCounterEngine = new WordCounterEngine();

	@Test
	public void testCountWordFrequencyOfOccurrence() throws Exception {
		failedTestCaseList.add("TestWordCounterEngine.testCountWordFrequencyOfOccurrence()");
		
		Map<String, Integer> wordCounterMap = new HashMap<String, Integer>();
		wordCounterMap.put("chair", 2);
		wordCounterMap.put("table", 1);
		wordCounterMap.put("bottle", 3);

		// Test case 1
		int count = wordCounterEngine.countWordFrequencyOfOccurrence("chair", wordCounterMap);

		assertEquals(2, count);

		// Test case 2 - when word doesn't exist in map
		count = wordCounterEngine.countWordFrequencyOfOccurrence("wallet", wordCounterMap);

		assertEquals(0, count);
		
		failedTestCaseList.add("TestWordCounterEngine.testCountWordFrequencyOfOccurrence()");
	}
}
