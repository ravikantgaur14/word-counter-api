package com.wordcounter.processor.services;

import static org.junit.Assert.assertEquals;
import java.util.Map;
import org.junit.Test;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import com.wordcounter.processor.base.TestBaseSetup;
import com.wordcounter.processor.response.WordContainerResponse;
import com.wordcounter.processor.utils.WordCounterEngine;

public class TestWordContainerProcessorService extends TestBaseSetup {

	WordContainerProcessorService wordContainerProcessorService = new WordContainerProcessorService();

	@Test
	public void testUpdateWordContainer() throws Exception {
		failedTestCaseList.add("TestWordContainerProcessorService.testUpdateWordContainer()");

		wordContainerProcessorService.setGoogleApiKeyForGoogleColudAPIAccess(googleApiKey);
		wordContainerProcessorService.setWordContainerComposer(wordContainerComposer);
		String word = "flor";
		String expectedRetuen = "flower";

		PowerMockito.when(wordTranslatorMock.googleTranslatorApiLookUpToWordIntoEnglish(Mockito.anyString(),
				Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn(expectedRetuen);

		// Test case 1 -> when word translation is not present in word container
		wordContainerProcessorService.updateWordContainer(word);
		int count = WordCounterEngine.countWordFrequencyOfOccurrence("flor",
				wordContainerComposer.getEnglishWordContainer());
		assertEquals(1, count);
		assertEquals(true, wordContainerComposer.getWordTranslationContainer().containsKey(word));

		// Test case 2 -> when word translation is already present in word container,
		// don't look-up in google translator api
		wordContainerProcessorService.updateWordContainer(word);
		count = WordCounterEngine.countWordFrequencyOfOccurrence("flor",
				wordContainerComposer.getEnglishWordContainer());
		assertEquals(2, count);

		assertEquals(true, wordContainerComposer.getWordTranslationContainer().containsKey(word));

		failedTestCaseList.remove("TestWordContainerProcessorService.testUpdateWordContainer()");
	}

	@Test
	public void testCountWordIntoWordContainer() throws Exception {
		failedTestCaseList.add("TestWordContainerProcessorService.testCountWordIntoWordContainer()");

		// Test case 1 -> when word is never added in word container
		String word = "medicine";
		String expectedRetuen = "medicine";

		PowerMockito.when(wordTranslatorMock.googleTranslatorApiLookUpToWordIntoEnglish(Mockito.anyString(),
				Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn(expectedRetuen);

		String actualReturnMessage = wordContainerProcessorService.countWordIntoWordContainer("medicine");
		String expectedReturnMessage = String.format(
				"Translated value for the given word \'%s\' is \'%s\' and this translated value is not found in Word Container. ",
				word, expectedRetuen);
		assertEquals(expectedReturnMessage, actualReturnMessage);

		// Test case 2 -> when word is not present in translation word container, but
		// english version is already added before
		// here we will go google translation api look-up key english value
		word = "botella";
		expectedRetuen = "bottle";

		PowerMockito.when(wordTranslatorMock.googleTranslatorApiLookUpToWordIntoEnglish(Mockito.anyString(),
				Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn(expectedRetuen);

		actualReturnMessage = wordContainerProcessorService.countWordIntoWordContainer(word);
		expectedReturnMessage = String.format(
				"Frequency of occurrence of the given word \'%s\' is \'%s\' in Word Counter Container. "
						+ " This word has been added into Word Container as translated value \'%s\'",
				word, 1, expectedRetuen);

		assertEquals(expectedReturnMessage, actualReturnMessage);

		// Test case 3 -> when word is already present in translation word container,
		// we will not look-up in google translator api, will get english value from
		// container
		word = "bandeja";
		expectedRetuen = "tray";

		actualReturnMessage = wordContainerProcessorService.countWordIntoWordContainer(word);
		expectedReturnMessage = String.format(
				"Frequency of occurrence of the given word \'%s\' is \'%s\' in Word Counter Container. "
						+ " This word has been added into Word Container as translated value \'%s\'",
				word, 1, expectedRetuen);
		assertEquals(expectedReturnMessage, actualReturnMessage);

		failedTestCaseList.remove("TestWordContainerProcessorService.testCountWordIntoWordContainer()");
	}

	@Test
	public void testViewWordContainerSnapshot() throws Exception {
		failedTestCaseList.add("TestWordContainerProcessorService.testViewWordContainerSnapshot()");

		WordContainerResponse wordContainerResponse = wordContainerProcessorService.viewWordContainerSnapshot();

		Map<String, Integer> englishWordContainerSnapShot = wordContainerResponse.getEnglishWordContainerSnapShot();
		assertEquals(true, englishWordContainerSnapShot.containsKey("chair"));
		assertEquals(true, englishWordContainerSnapShot.containsKey("bottle"));
		assertEquals(true, englishWordContainerSnapShot.containsKey("tray"));

		Map<String, String> translatedWordContainerSnapShot = wordContainerResponse.getTranslatedWordContainerSnapShot();
		assertEquals(true, translatedWordContainerSnapShot.containsKey("silla"));
		assertEquals(true, translatedWordContainerSnapShot.containsKey("bottle"));
		assertEquals(true, translatedWordContainerSnapShot.containsKey("bandeja"));

		assertEquals(true, (wordContainerResponse.getMessage() == null));

		failedTestCaseList.remove("TestWordContainerProcessorService.testViewWordContainerSnapshot()");
	}

}
