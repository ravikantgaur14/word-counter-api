package com.wordcounter.processor.base;

import java.util.ArrayList;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.wordcounter.processor.bean.WordContainerComposer;
import com.wordcounter.processor.utils.WordTranslator;

@RunWith(PowerMockRunner.class)
public class TestBaseSetup {

	static Logger log = LoggerFactory.getLogger(TestBaseSetup.class);
	public static WordContainerComposer wordContainerComposer = new WordContainerComposer();
	public static ArrayList<String> failedTestCaseList = new ArrayList<String>();
	public static WordTranslator wordTranslatorSpyMock=null;
	public static WordTranslator wordTranslatorMock=null;
	public static String from = "";
	public static String to = "en";
	public static String googleApiKey = "DUMMY_API_KEY_STRING";
	
	@BeforeClass
	public static void initialTestSetup() {
		failedTestCaseList.add("TestBaseSetup.initialTestSetup()");
		
		wordContainerComposer.getEnglishWordContainer().put("chair", 1);
		wordContainerComposer.getEnglishWordContainer().put("bottle", 1);
		wordContainerComposer.getEnglishWordContainer().put("tray", 1);
		
		wordContainerComposer.getWordTranslationContainer().put("silla", "chair");
		wordContainerComposer.getWordTranslationContainer().put("bottle", "bottle");
		wordContainerComposer.getWordTranslationContainer().put("bandeja", "tray");
		
		MockitoAnnotations.initMocks(WordTranslator.class);
		wordTranslatorSpyMock = PowerMockito.spy(new WordTranslator());
		wordTranslatorMock = PowerMockito.mock(WordTranslator.class);
		PowerMockito.mockStatic(WordTranslator.class);
	
		failedTestCaseList.remove("TestBaseSetup.initialTestSetup()");
	}

	@AfterClass
	public static void afterTestExecution() {
		if(failedTestCaseList.isEmpty()) {
			log.info("All test cases has been executed successfully.");
		}else {
			log.info("All test has been executed. But following test cases got failed : \n" + failedTestCaseList.toString());
		}
		
	}
}
