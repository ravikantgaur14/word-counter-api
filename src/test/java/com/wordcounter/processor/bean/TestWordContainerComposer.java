package com.wordcounter.processor.bean;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import com.wordcounter.processor.base.TestBaseSetup;
import com.wordcounter.processor.utils.WordCounterEngine;

public class TestWordContainerComposer extends TestBaseSetup{
	
	@Test
	public void testPut() throws Exception {
		failedTestCaseList.add("TestWordContainerComposer.testPut()");
		
		wordContainerComposer.put("chair", "chair");
		int count = WordCounterEngine.countWordFrequencyOfOccurrence("chair", wordContainerComposer.getEnglishWordContainer());
		assertEquals(2, count);
		
		wordContainerComposer.put("book", "book");
		count = WordCounterEngine.countWordFrequencyOfOccurrence("book", wordContainerComposer.getEnglishWordContainer());
		assertEquals(1, count);
		
		assertEquals(true, wordContainerComposer.getWordTranslationContainer().containsKey("book"));
		assertEquals(true, wordContainerComposer.getWordTranslationContainer().containsKey("chair"));
		
		failedTestCaseList.remove("TestWordContainerComposer.testPut()");

	}
	
}
