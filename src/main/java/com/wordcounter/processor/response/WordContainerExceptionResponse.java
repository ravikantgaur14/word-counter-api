/**
 * 
 */
package com.wordcounter.processor.response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.wordcounter.processor.exception.WordContainerException;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Ravi.Gaur
 *
 */
@Component
@Data
@Slf4j
@JsonInclude(Include.NON_NULL)
@JsonSerialize
@JsonPropertyOrder({"errorMessage", "requestStatusCode"})
public class WordContainerExceptionResponse extends WordContainerException implements iWordContainerResponse {

	Logger log = LoggerFactory.getLogger(WordContainerExceptionResponse.class);
	private static final long serialVersionUID = 1L;

    String errorMessage;
	int requestStatusCode;
	
	public WordContainerExceptionResponse() {
		super();
	}

	public WordContainerExceptionResponse(String errorMessage) {
		super(errorMessage);
	}
	
	public WordContainerExceptionResponse(String errorMessage, Exception ex) {
		super(errorMessage, ex);
	}

	@Override
	public void buildResponse(String errorMessage, int requestStatusCode) {
		this.errorMessage = errorMessage;
		this.requestStatusCode = requestStatusCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public int getRequestStatusCode() {
		return requestStatusCode;
	}

	public void setRequestStatusCode(int requestStatusCode) {
		this.requestStatusCode = requestStatusCode;
	}
	
}
