/**
 * 
 */
package com.wordcounter.processor.response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Ravi.Gaur
 *
 */
@Component
@Data
@Slf4j
@JsonInclude(Include.NON_NULL)
@JsonSerialize
@JsonPropertyOrder({"errorMessage", "requestStatusCode"})
public class WordContainerErrorResponse implements iWordContainerResponse {

	Logger log = LoggerFactory.getLogger(WordContainerErrorResponse.class);
	private static final long serialVersionUID = 1L;

    String errorMessage;
	int requestStatusCode;
	@JsonIgnore
	WordContainerExceptionResponse wordContainerExceptionResponse;
	
	public WordContainerErrorResponse() {

	}

	public WordContainerErrorResponse(String errorMessage, int requestStatusCode, Exception ex) {
		buildResponse(errorMessage, requestStatusCode);
		wordContainerExceptionResponse = new WordContainerExceptionResponse(errorMessage, ex);
	}

	@Override
	public void buildResponse(String errorMessage, int requestStatusCode) {
		this.errorMessage = errorMessage;
		this.requestStatusCode = requestStatusCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public int getRequestStatusCode() {
		return requestStatusCode;
	}

	public void setRequestStatusCode(int requestStatusCode) {
		this.requestStatusCode = requestStatusCode;
	}
	
}
