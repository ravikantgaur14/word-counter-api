package com.wordcounter.processor.response;

/**
 * @author Ravi.Gaur
 *
 */

public interface iWordContainerResponse {
	
	public void buildResponse(String mggessage, int statusCode);
}
