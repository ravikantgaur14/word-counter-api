package com.wordcounter.processor.response;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Ravi.Gaur
 *
 */

@NoArgsConstructor
@Component
@Data
@Slf4j
@JsonInclude(Include.NON_NULL)
@JsonSerialize
@JsonPropertyOrder({"message", "englishWordContainerSnapShot", "translatedWordContainerSnapShot", "requestStatusCode"})
public class WordContainerResponse implements iWordContainerResponse {

	Logger log = LoggerFactory.getLogger(WordContainerResponse.class);
	private static final long serialVersionUID = 1L;
	
	String message=null;
	Map<String, Integer> englishWordContainerSnapShot = null;
	Map<String, String> translatedWordContainerSnapShot = null;
	int requestStatusCode;
	
	@Override
	public void buildResponse(String message, int statusCode) {
		this.message = message;
		this.requestStatusCode = statusCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getRequestStatusCode() {
		return requestStatusCode;
	}

	public void setRequestStatusCode(int requestStatusCode) {
		this.requestStatusCode = requestStatusCode;
	}

	public Map<String, Integer> getEnglishWordContainerSnapShot() {
		return englishWordContainerSnapShot;
	}

	public void setEnglishWordContainerSnapShot(Map<String, Integer> englishWordContainerSnapShot) {
		this.englishWordContainerSnapShot = englishWordContainerSnapShot;
	}

	public Map<String, String> getTranslatedWordContainerSnapShot() {
		return translatedWordContainerSnapShot;
	}

	public void setTranslatedWordContainerSnapShot(Map<String, String> translatedWordContainerSnapShot) {
		this.translatedWordContainerSnapShot = translatedWordContainerSnapShot;
	}

}
