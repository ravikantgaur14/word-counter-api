package com.wordcounter.processor.filter;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.wordcounter.processor.entity.Word;
import com.wordcounter.processor.response.WordContainerErrorResponse;
import com.wordcounter.processor.utils.WordContainerUtils;

@WebFilter( urlPatterns = { "/word/container/addWordService", "/word/container/countWordService" })
@Component
public class WordContainerProcessorFilter implements Filter {

	Logger log = LoggerFactory.getLogger(WordContainerProcessorFilter.class);
	private List<String> excludedUrls = Arrays.asList((new String[]{"/word/container/viewWordContainer"}));

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		WordContainerProcessorRequestWrapper wordContainerProcessorRequestWrapper 
                                                 = new WordContainerProcessorRequestWrapper(request);
		String path = wordContainerProcessorRequestWrapper.getServletPath();
		boolean byPassRequestValidation = byPassRequestValidationForExcludedUrls(path);
		
		if(!byPassRequestValidation) {
			HttpServletResponse httpServletResponse = (HttpServletResponse) response;
			ObjectMapper mapper = new ObjectMapper();

			try {
				
				String requestPayload = wordContainerProcessorRequestWrapper.getRequestBody();
				Gson gson = new Gson();
				Word word = gson.fromJson(requestPayload, Word.class);

				if (!(word != null && word.getWord() != null && word.getWord().length() != 0
						&& WordContainerUtils.isAlphabeticWord(word.getWord()))) {

					log.error("Given input word \'{}\' is invalid. We will not proceed to the Word Container services", word.getWord());
					String erorMessage = "Invalid word. Only alphabetic words are excepted to add or look-up into Word Container. Empty & null values are invalid.";
					throw new Exception(erorMessage);

				} else {
					log.info("Given input word \'{}\' is valid. We will proceed to the Word Container services", word.getWord());
					chain.doFilter(wordContainerProcessorRequestWrapper, response);
				}
			} catch (Exception ex) {
				WordContainerErrorResponse wordContainerErrorResponse = new WordContainerErrorResponse(ex.getMessage(), HttpServletResponse.SC_FORBIDDEN, ex);
				httpServletResponse.setContentType(MediaType.APPLICATION_JSON_VALUE);
				httpServletResponse.setStatus(HttpServletResponse.SC_FORBIDDEN);
				httpServletResponse.getWriter().write(mapper.writeValueAsString(wordContainerErrorResponse));
			}
		}else {
			chain.doFilter(wordContainerProcessorRequestWrapper, response);
		}
	}

	protected boolean byPassRequestValidationForExcludedUrls(String path) {
		if (excludedUrls.contains(path)) {
			return true;
		}
		return false;
	}

}
