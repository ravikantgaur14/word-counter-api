package com.wordcounter.processor.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.wordcounter.processor.bean.WordContainerComposer;
import com.wordcounter.processor.response.WordContainerResponse;
import com.wordcounter.processor.utils.WordContainerUtils;
import com.wordcounter.processor.utils.WordCounterEngine;
import lombok.Data;

/**
 * @author Ravi.Gaur
 *
 */
@Service
@Data
public class WordContainerProcessorService {

	Logger log = LoggerFactory.getLogger(WordContainerProcessorService.class);

	@Value("${google-api-key}")
	String googleApiKeyForGoogleColudAPIAccess;

	@Autowired
	WordContainerComposer wordContainerComposer;

	public String updateWordContainer(String word) throws Exception {

		String englishTranslatedWord = WordContainerUtils.wordTranslatoinLookUp(word, wordContainerComposer, 
				                                                                          googleApiKeyForGoogleColudAPIAccess);
		log.info("Updating given word into Word Container.");
		wordContainerComposer.put(word, englishTranslatedWord);
		return englishTranslatedWord;
	}

	public String countWordIntoWordContainer(String word) throws Exception {
		String englishTranslatedWord = WordContainerUtils.wordTranslatoinLookUp(word, wordContainerComposer, 
				                                                                        googleApiKeyForGoogleColudAPIAccess);
		int count = WordCounterEngine.countWordFrequencyOfOccurrence(englishTranslatedWord,
				wordContainerComposer.getEnglishWordContainer());
		String wordCountMessage="";
		if(count == 0) {
			wordCountMessage = String.format("Translated value for the given word \'%s\' is \'%s\' and this translated value is not found in Word Container. ", word, englishTranslatedWord);
			log.info(wordCountMessage);
		}else {
			wordCountMessage = String.format("Frequency of occurrence of the given word \'%s\' is \'%s\' in Word Counter Container. "
					+ " This word has been added into Word Container as translated value \'%s\'", word, count, englishTranslatedWord);
			log.info(wordCountMessage);
		}
		return wordCountMessage;
	}

	public WordContainerResponse viewWordContainerSnapshot() throws Exception {
		WordContainerResponse wordContainerResponse = new WordContainerResponse();
		wordContainerResponse.setMessage(null);
		wordContainerResponse.setEnglishWordContainerSnapShot(wordContainerComposer.getEnglishWordContainer());
		wordContainerResponse.setTranslatedWordContainerSnapShot(wordContainerComposer.getWordTranslationContainer());
		return wordContainerResponse;
	}

	
	public String getGoogleApiKeyForGoogleColudAPIAccess() {
		return googleApiKeyForGoogleColudAPIAccess;
	}

	public void setGoogleApiKeyForGoogleColudAPIAccess(String googleApiKeyForGoogleColudAPIAccess) {
		this.googleApiKeyForGoogleColudAPIAccess = googleApiKeyForGoogleColudAPIAccess;
	}

	public WordContainerComposer getWordContainerComposer() {
		return wordContainerComposer;
	}

	public void setWordContainerComposer(WordContainerComposer wordContainerComposer) {
		this.wordContainerComposer = wordContainerComposer;
	}
	
}
