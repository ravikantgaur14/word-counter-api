/**
 * 
 */
package com.wordcounter.processor.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Ravi.Gaur
 *
 */

public class WordContainerException extends Exception {

	Logger log = LoggerFactory.getLogger(WordContainerException.class);
	private static final long serialVersionUID = 1L;

	String exceptionMessage;
	
	public WordContainerException() {
		super();
	}

	public WordContainerException(String errorMessage) {
		super(errorMessage);
		this.exceptionMessage = errorMessage;
		logExceptionAtLogConsole();
	}
	
	public WordContainerException(String errorMessage, Exception ex) {
		 super(errorMessage, ex);
		 this.exceptionMessage = errorMessage;
		logExceptionAtLogConsole(ex);
	}

	private void logExceptionAtLogConsole() {
		log.error(this.exceptionMessage);
	}
	
	private void logExceptionAtLogConsole(Exception ex) {
		log.error(this.exceptionMessage, ex);
	}
	
}
