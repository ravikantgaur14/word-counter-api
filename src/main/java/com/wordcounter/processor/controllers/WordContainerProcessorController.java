package com.wordcounter.processor.controllers;

import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.wordcounter.processor.entity.Word;
import com.wordcounter.processor.response.WordContainerErrorResponse;
import com.wordcounter.processor.response.WordContainerResponse;
import com.wordcounter.processor.response.iWordContainerResponse;
import com.wordcounter.processor.services.WordContainerProcessorService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Ravi.Gaur
 *
 */
@RestController
@RequestMapping("/word/container")
@Slf4j
@Api("Word count library is a Word Container service which offers you adding and look-up of word into word container. It uses "
		+ " Google Cloud Translation API internally which requires project api key, which you can generate via login to "
		+ "https://console.developers.google.com/apis/api/translate.googleapis.com/overview"
		+ " once you have key genrated you can set it to application.properties")
public class WordContainerProcessorController {

	Logger log = LoggerFactory.getLogger(WordContainerProcessorController.class);

	@Autowired
	WordContainerProcessorService wordContainerProcessorService;
	@Autowired
	WordContainerResponse wordContainerResponse;

	@RequestMapping(value = "/addWordService", consumes = { "application/json" }, produces = {
			"application/json" }, method = RequestMethod.PUT)
	@ResponseBody
	@ApiOperation(value = "Add the word into Word Container and return the add message confirmation, "
			+ "if word value is non-alphabetic characters then return exception. "
			+ "It uses google translator API to manage same meaning word in english lanugaue", notes = "This API method addWord() excpet the JSON with key-value as key word and it's input as value, and add the word's english translation to Word Container"
					+ " if given word has only alphabetic characters.")
	public ResponseEntity<iWordContainerResponse> addWord(@RequestBody Word word) throws Exception {
		try {

			String englishTranslatedWord = wordContainerProcessorService.updateWordContainer(word.getWord());

			String messageLog = String.format(
					"Word \'%s\' has been added into Word Container as english translated value \'%s\'", word.getWord(),
					englishTranslatedWord);
			log.info(messageLog);
			wordContainerResponse.buildResponse(messageLog, HttpServletResponse.SC_OK);
			return ResponseEntity.status(HttpStatus.OK).body(wordContainerResponse);

		} catch (Exception ex) {
			String errorMessage = String.format(
					"Error occurred while adding word \'%s\' into Word Container. " + "Please see system log.",
					word.getWord());
			WordContainerErrorResponse wordContainerErrorResponse = new WordContainerErrorResponse(errorMessage,
					HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex);

			return new ResponseEntity<iWordContainerResponse>(wordContainerErrorResponse,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@RequestMapping(value = "/countWordService", consumes = { "application/json" }, produces = {
			"application/JSON" }, method = RequestMethod.PUT)
	@ApiOperation(value = "Get the word count from Word Container and return the count message confirmation, "
			+ "if word value is non-alphabetic characters then return exception. "
			+ "It uses google translator API to manage same meaning word in english lanugaue", notes = "This API method getWordCount() excpet the JSON with key-value as key word and it's input as value and count the word into the Word Container if it given word is contains only alphabetic characters and added to word conatiner successfully.")
	public ResponseEntity<iWordContainerResponse> countWord(@RequestBody Word word) throws Exception {

		try {

			String wordCountMessage = wordContainerProcessorService.countWordIntoWordContainer(word.getWord());
			log.info(wordCountMessage);
			wordContainerResponse.buildResponse(wordCountMessage, HttpServletResponse.SC_OK);
			return ResponseEntity.status(HttpStatus.OK).body(wordContainerResponse);

		} catch (Exception ex) {
			String errorMessage = "Error occurred while counting word frequency of occurrance into Word Container. "
					+ "Please see system log.";
			WordContainerErrorResponse wordContainerErrorResponse = new WordContainerErrorResponse(errorMessage,
					HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex);

			return new ResponseEntity<iWordContainerResponse>(wordContainerErrorResponse,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@RequestMapping(value = "/viewWordContainer", produces = { "application/JSON" }, method = RequestMethod.GET)
	@ApiOperation(value = "View Word Container", notes = "View Word Container to see counts of words added there")
	public ResponseEntity<iWordContainerResponse> viewWordContainerSnapshot() throws Exception {
		try {

			WordContainerResponse wordContainerResponse = wordContainerProcessorService.viewWordContainerSnapshot();

			log.info("English Word Container Snapshot is \n {} ",
					wordContainerResponse.getEnglishWordContainerSnapShot().toString());
			log.info("Translated Word Container Snapshot is \n {} ",
					wordContainerResponse.getTranslatedWordContainerSnapShot().toString());
			wordContainerResponse.setRequestStatusCode(HttpServletResponse.SC_OK);
			return ResponseEntity.status(HttpStatus.OK).body(wordContainerResponse);

		} catch (Exception ex) {
			String errorMessage = "Error occurred while viewing Word Container. Please see system log.";
			WordContainerErrorResponse wordContainerErrorResponse = new WordContainerErrorResponse(errorMessage,
					HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex);

			return new ResponseEntity<iWordContainerResponse>(wordContainerErrorResponse,
					HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

}
