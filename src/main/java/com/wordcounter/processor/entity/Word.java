package com.wordcounter.processor.entity;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Ravi.Gaur
 *
 */
@Data
@NoArgsConstructor
public class Word {

	@JsonProperty("word")
	public String word;

	public String getWord() {
		return word.trim().toLowerCase();
	}

	public void setWord(String word) {
		this.word = word;
	}

}
