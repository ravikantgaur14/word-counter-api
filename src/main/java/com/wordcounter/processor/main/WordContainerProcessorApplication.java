package com.wordcounter.processor.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author Ravi.Gaur
 *
 */
@SpringBootApplication
@EnableSwagger2
@ComponentScan(basePackages="com.wordcounter.processor")
public class WordContainerProcessorApplication {

	public static void main(String[] args) {
		SpringApplication.run(WordContainerProcessorApplication.class, args);
	}

}
