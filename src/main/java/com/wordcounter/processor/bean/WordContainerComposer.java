package com.wordcounter.processor.bean;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


/**
 * @author Ravi.Gaur
 *
 */

@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class WordContainerComposer {

	 Map<String, Integer> englishWordContainer = new HashMap<String, Integer>();
	 
	 Map<String, String> wordTranslationContainer = new HashMap<String, String>();
	 
	 public void put(String originalWord, String englishTranslatedWord) {
		
		 if(!(englishWordContainer.containsKey(englishTranslatedWord))) {
			 englishWordContainer.put(englishTranslatedWord, 1);
		 }else {
			 //update the word counter
			 englishWordContainer.computeIfPresent(englishTranslatedWord, ((k, v) -> (v + 1)));
		 }
		 
		 wordTranslationContainer.put(originalWord, englishTranslatedWord);
	 }

	public Map<String, Integer> getEnglishWordContainer() {
		return englishWordContainer;
	}

	public void setEnglishWordContainer(Map<String, Integer> englishWordContainer) {
		this.englishWordContainer = englishWordContainer;
	}

	public Map<String, String> getWordTranslationContainer() {
		return wordTranslationContainer;
	}

	public void setWordTranslationContainer(Map<String, String> wordTranslationContainer) {
		this.wordTranslationContainer = wordTranslationContainer;
	}

}
