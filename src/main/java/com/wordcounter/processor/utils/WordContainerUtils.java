package com.wordcounter.processor.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.wordcounter.processor.bean.WordContainerComposer;

public class WordContainerUtils {

	static Logger log = LoggerFactory.getLogger(WordContainerUtils.class);

	public static boolean isAlphabeticWord(String word) {
		if (word != null && word.length() != 0) {
			String regex = "^[a-zA-Z]*$";
			Pattern pattern = Pattern.compile(regex);
			Matcher matcher = pattern.matcher(word);
			if (matcher.matches()) {
				return true;
			}
		}
		return false;
	}

	public static String wordTranslatoinLookUp(String word, WordContainerComposer wordContainerComposer,
			String googleApiKeyForGoogleColudAPIAccess) throws Exception {

		String englishTranslatedWord = "";
		/*
		 * If word has been previously translated and it's translation present in Word
		 * Translation Container already, then no need to look-up at Google translation
		 * api gain to get same translation
		 */
		if (wordContainerComposer.getWordTranslationContainer().containsKey(word)) {
			englishTranslatedWord = wordContainerComposer.getWordTranslationContainer().get(word);
		} else {
			// google translation api calling
			log.info("Invoking Google translator service");
			englishTranslatedWord = WordTranslator.getTraslatedEnglishWordForNonEnglishWord(word,
					googleApiKeyForGoogleColudAPIAccess);
			log.info("Google translator translated word {} as {}", word, englishTranslatedWord);
		}

		return englishTranslatedWord.trim().toLowerCase();
	}
}
