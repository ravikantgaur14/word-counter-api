package com.wordcounter.processor.utils;

import java.util.Map;

/**
 * @author Ravi.Gaur
 *
 */
public class WordCounterEngine {

	public static int countWordFrequencyOfOccurrence(String word, Map<String, Integer> wordCounterMap)
			throws Exception {
		if(wordCounterMap.get(word)!=null) {
			return wordCounterMap.get(word).intValue();
		}
		return 0;
	}
}
