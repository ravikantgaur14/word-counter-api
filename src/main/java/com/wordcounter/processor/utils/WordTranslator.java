package com.wordcounter.processor.utils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import javax.net.ssl.HttpsURLConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * @author Ravi.Gaur
 *
 */
public class WordTranslator {

	static Logger log = LoggerFactory.getLogger(WordTranslator.class);

	public static String getTraslatedEnglishWordForNonEnglishWord(String word,
			String googleApiKeyForGoogleColudAPIAccess) throws Exception {
		
		// pass source language as "" for auto detection of language
		String traslatedEnglishWord = googleTranslatorApiLookUpToWordIntoEnglish(word, "", "en", googleApiKeyForGoogleColudAPIAccess);
		boolean errorCheckCondition1 = word!=null && traslatedEnglishWord==null ;
		boolean errorCheckCondition2 = word!=null && "".equals(traslatedEnglishWord);

		if(errorCheckCondition1 || errorCheckCondition2) {
			throw new Exception("Google translation api translated  null or empty value for given word '" + word + "'");
		}
		return traslatedEnglishWord;
	}

	public static String googleTranslatorApiLookUpToWordIntoEnglish(String text, String from, String to,
			String googleApiKeyForGoogleColudAPIAccess) throws Exception{
		StringBuilder result = new StringBuilder();
		try {
			String encodedText = URLEncoder.encode(text, "UTF-8");
			String urlStr = "https://www.googleapis.com/language/translate/v2?key="
					+ googleApiKeyForGoogleColudAPIAccess + "&q=" + encodedText + "&target=" + to + "&source=" + from;

			URL url = new URL(urlStr);

			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			InputStream stream;
			if (conn.getResponseCode() == 200) // success
			{
				stream = conn.getInputStream();
			} else
				stream = conn.getErrorStream();

			BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
			String line;
			while ((line = reader.readLine()) != null) {
				result.append(line);
			}

			JsonParser parser = new JsonParser();

			JsonElement element = parser.parse(result.toString());

			if (element.isJsonObject()) {
				JsonObject obj = element.getAsJsonObject();
				if (obj.get("error") == null) {
					String translatedText = obj.get("data").getAsJsonObject().get("translations").getAsJsonArray()
							.get(0).getAsJsonObject().get("translatedText").getAsString();
					return translatedText;

				}
			}

			if (conn.getResponseCode() != 200) {
				System.err.println(result);
			}

		} catch (Exception ex) {
			System.err.println(ex.getMessage());
		}

		return null;
	}

}
