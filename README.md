# README #

Word Count library is a Word Container service which offers you adding and look-up of word into word container. It uses Google Cloud Translation API internally which requires project api key for authentication, which you can generate via login to google developer console "https://console.developers.google.com/apis/api/translate.googleapis.com/overview", once you generate api-key. You should set it to application.properties

### What is this repository for? ###

* Quick summary
  Word Count library is a Word Container service which offers you adding and look-up of word into word container. This has following rest service endpoints exposed which can be as microservices in distributed system:
  
####  Add new word into Word Container ####
      * Add the word into Word Container and return the add message confirmation, if word value is non-alphabetic characters then return exception. It uses google translator API to manage same meaning word in english lanugaue. This API endpoint excpet the JSON with key-value as key word and it's input as value, and add the word's english translation to Word Container if given word has only alphabetic characters.
	
      Endpoint -> PUT: http://<host>:<port>/wordCounter/word/container/addWordService
	  Example:
	  Request Payload: 
	  {
        "word": "kerusi"
      }
	  Response Payload:
	  {
        "message": "Word 'kerusi' has been added into Word Container as english translated value 'chair'",
        "requestStatusCode": 200
      }
	  
####  Count word into Word Container ####
      * Get the word count from Word Container and return the count message confirmation, if word value is non-alphabetic characters then return exception. It uses google translator API to manage same meaning word in english lanugaue. This API endpoint excpet the JSON with key-value as key word and it's input as value and count the word into the Word Container if it given word is contains only alphabetic characters and added to word conatiner successfully.
	
      Endpoint -> PUT: http://<host>:<port>/wordCounter/word/container/countWordService
	  Example:
      Request Payload: 
	  {
        "word": "kerusi"
      }
      Response Payload:
	  {
        "message": "Frequency of occurrence of the given word 'kerusi' is '1' in Word Counter Container. This word has been added into Word Container as translated value 'chair'",
        "requestStatusCode": 200
      }

####  View SnapShot Of Word Container ####
      Endpoint -> GET: http://<host>:<port>/wordCounter/word/container/viewWordContainer
	  Example:
	  Response Payload:
	  {
        "englishWordContainerSnapShot": {
											"chair": 2,
											"table": 1
										},
		"translatedWordContainerSnapShot": {
												"chair": "chair",
												"kerusi": "chair",
												"table": "table"
											},
		"requestStatusCode": 200
	  }
	  
	Note: this api has request filter enable which helps to reduce unwanted load on server container. If given word is null, empty of non-alphabetic then request will get filter before sending to server container (Dispatcher Servlet). Example:
    Response Payload:
    {
     "word": "chair@34"
    }	
	Response Payload:
	{
      "errorMessage": "Invalid word. Only alphabetic words are excepted to add or look-up into Word Container. Empty & null values are invalid.",
      "requestStatusCode": 403
    }
	
### How do I get set up? ###

* Summary of set up
  It is maven project, You need to import it as maven project and once improted do clean and build after checking out this project at loacal.
* Configuration
  Word Count library uses Google Cloud Translation API internally which requires project api key for authentication, which you can generate via login to google developer console https://console.developers.google.com/apis/api/translate.googleapis.com/overview, once you generate api-key. You should set it to application.properties
  properties:  google-api-key=AIzaSyASzTr1ZtEO8MZiDObftSfmBCrzl0BJ6Tw
* Dependencies
  Google Cloud Translation API :
  https://www.googleapis.com/language/translate/v2?key=<googleApiKeyForGoogleColudAPIAccess>&q=<word>&target=<language code>&source=<language code>
  
  Please refer url to check code: https://developers.google.com/admin-sdk/directory/v1/languages

* Database configuration
   N/A
* How to run tests
  Right click on Test package which is "word-counter-api\src\test\java" then run as Junit
* Deployment instructions
  Word Count library is a Spring Boot application to run this you can run follwoing file 
 word-counter-api/src/main/src/main/java/com/wordcounter/processor/main/WordContainerProcessorApplication.java

### Testing Evidence ###
Please refer to following folder which has testing evidence available.
 https://bitbucket.org/ravikantgaur14/word-counter-api/src/main/testing-evidence/